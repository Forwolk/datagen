package ru.lanit.test.datagen.config;

public final class Config {
    private Config (){}

    public final static int X_SIZE = 300;
    public final static int Y_SIZE = 200;
    public final static String TITLE = String.format("Генерация данных. Тестовое задание от ЛАНИТ");
}
