package ru.lanit.test.datagen;

import ru.lanit.test.datagen.data.DAO;
import ru.lanit.test.datagen.data.FileDAO;
import ru.lanit.test.datagen.gui.Window;
import ru.lanit.test.datagen.thread.ThreadManager;

import java.io.IOException;

public class Main {

    public static void main (String... args) {
        createTimer();
        Window mainWindow = Window.getInstance();
        mainWindow.setVisible(true);
    }


    /**
     * Создание таймера, который раз в 10 секунд будет сохранять данные
     */
    private static void createTimer () {
        Runnable r = () -> {
            while (true) {
                final DAO dao = FileDAO.getInstance();
                while (!dao.isChanged()) {} //Ожидание изменений
                synchronized (dao) {
                    try {
                        dao.save();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        ThreadManager.getInstance().execute(r);
    }
}
