package ru.lanit.test.datagen.data;

import java.io.IOException;

public interface DAO {

    /**
     * Добавление n случайных элементов
     *
     * @param n - количество добавляемых элементов
     */
    void createElements (int n);

    /**
     * Удаление n случайных элементов
     *
     * @param n - количество удаляемых элементов
     */
    void deleteElements (int n);

    /**
     * Проверка, изменилось ли что-либо с момента предыдущего сохранения
     *
     * @return да/нет
     */
    boolean isChanged ();

    /**
     * Сохранить объекты
     */
    void save () throws IOException;
}
