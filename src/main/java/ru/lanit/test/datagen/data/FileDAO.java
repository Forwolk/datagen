package ru.lanit.test.datagen.data;

import ru.lanit.test.datagen.thread.ThreadManager;
import ru.lanit.test.datagen.gui.Window;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class FileDAO implements DAO, Serializable {
    private final ConcurrentHashMap <String, Double> information = new ConcurrentHashMap<>();
    private boolean changed = true;

    private FileDAO () {}

    private static FileDAO instance;

    /**
     * Синглтон
     *
     * @return экземпляр FileDAO
     */
    public static FileDAO getInstance () {
        if (instance == null)
            instance = new FileDAO();
        return instance;
    }

    /**
     * Добавление n случайных элементов
     *
     * @param n - количество добавляемых элементов
     */
    public void createElements(final int n) {
        Runnable task = () -> {
            changed = true;
            for (int i = 0; i < n; i ++)
                information.put(generateKey(), generateValue());
        };

        ThreadManager.getInstance().execute(task);
    }

    /**
     * Удаление n случайных элементов
     *
     * @param n - количество удаляемых элементов
     */
    public void deleteElements(final int n) {
        Runnable task = () -> {
            changed = true;
            if (n >= information.size()) { //Проверка. Если требуется удалить не меньше данных чем есть, то удаляются все имеющиеся данные
                information.clear();
                return;
            }
            for (int i = 0; i < n; i ++)
                information.remove(getRandomKey());
        };

        ThreadManager.getInstance().execute(task);
    }

    private String getRandomKey () {
        String key;
        List<String> keys = new ArrayList<String>(information.keySet());
        key = keys.get((int) (Math.random() * (keys.size() - 1)));
        return key;
    }

    /**
     * Проверка, изменилось ли что-либо с момента предыдущего сохранения
     *
     * @return да/нет
     */
    public boolean isChanged() {
        return changed;
    }

    /**
     * Сохранить объекты в файл
     */
    public void save() throws IOException {
        Window window = Window.getInstance();

        changed = false;

        FileOutputStream fos = new FileOutputStream("information.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        //TODO: Sorting
        oos.writeObject(information); //TODO: Progress bar
        oos.flush();
        oos.close();
        fos.close();
        Window.getInstance().loadComplete();
    }

    private String generateKey () {
        long n = (long) (Long.MAX_VALUE * Math.random());
        StringBuilder sb = new StringBuilder();
        sb.append(n);
        for (int i = 0; i < 5; i ++) {
            char c = (char) (65 + 25 * Math.random());
            sb.append(c);
        }
        return sb.toString();
    }

    private double generateValue () {
        return Math.random();
    }
}
