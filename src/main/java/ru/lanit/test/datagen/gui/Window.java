package ru.lanit.test.datagen.gui;

import ru.lanit.test.datagen.config.Config;
import ru.lanit.test.datagen.thread.ThreadManager;
import ru.lanit.test.datagen.data.DAO;
import ru.lanit.test.datagen.data.FileDAO;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Window extends JFrame {

    private static Window instance;

    public static Window getInstance (){
        if (instance == null)
            instance = new Window();
        return instance;
    }

    private JPanel content = new JPanel();

    private JTextField numberField = new JTextField(10);
    private JButton addButton = new JButton("ADD");
    private JButton removeButton = new JButton("REMOVE");
    private JProgressBar saved = new JProgressBar();
    private JLabel time = new JLabel("%time%");

    private Window () {
        Dimension thisDimention = new Dimension(Config.X_SIZE, Config.Y_SIZE);
        this.setSize(thisDimention);
        this.setTitle(Config.TITLE);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.getContentPane().add(content);
        content.setBackground(Color.GRAY);
        content.setLayout(null);
        putComponents();
        setComponentsSizes();

        this.addComponentListener(new ComponentListener() {
            public void componentResized(ComponentEvent e) {
                Window.getInstance().setComponentsSizes();
            }

            public void componentMoved(ComponentEvent e) {

            }

            public void componentShown(ComponentEvent e) {

            }

            public void componentHidden(ComponentEvent e) {

            }
        });

        createTimer();
        addActions();
    }

    /**
     * Добавление объектов на панель
     */
    private void putComponents () {
        content.add(time);
        content.add(numberField);
        content.add(addButton);
        content.add(removeButton);
        content.add(saved);
    }

    /**
     * Изменение размеров и положений
     */
    private void setComponentsSizes () {
        int w = this.getWidth();
        int h = this.getHeight();

        // Позиция строки
        int linePos = (h - 160) / 2;
        // Отступ слева
        int indent = (int) (w * 0.1);

        time.setBounds(indent, linePos, (int) (w * 0.8), 25);
        time.setHorizontalAlignment(SwingConstants.CENTER);

        linePos += 35;

        numberField.setBounds(indent, linePos, (int) (w * 0.8), 25);
        numberField.setHorizontalAlignment(SwingConstants.CENTER);

        linePos += 35;

        int between = (int) (0.01 * w);
        addButton.setBounds(indent, linePos, (int) ((w * 0.8)/2 - between), 25);
        removeButton .setBounds((int) ((w * 0.8)/2 + between + w * 0.1), linePos, (int) ((w * 0.8)/2 - between), 25);

        linePos += 35;

        saved.setBounds(indent, linePos, (int) (w * 0.8), 25);
    }

    /**
     * Обновление информации о сохранении
     * @param current сколько записано
     * @param max сколько осталось
     */
    public void loading (int current, int max) {
        saved.setMaximum(max);
        saved.setValue(current);
    }

    /**
     * После завершения сохранения обнуляет ProgressBar
     */
    public void loadComplete () {
        saved.setValue(0);
    }

    /**
     * Создание таймера для обновления времени
     */
    private void createTimer () {
        Runnable r = () -> {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String time;
            while (true) {
                time = sdf.format(Calendar.getInstance().getTime());
                Window.getInstance().time.setText(time);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        ThreadManager.getInstance().execute(r);
    }

    private void addActions () {
        addButton.addActionListener(new FrameActionListener(Task.ADD));
        removeButton.addActionListener(new FrameActionListener(Task.REMOVE));
    }
    private enum Task {
        ADD,
        REMOVE
    }

    private class FrameActionListener implements ActionListener {
        final Task task;

        public FrameActionListener (Task task) {
            this.task = task;
        }

        /**
         * Invoked when an action occurs.
         *
         * @param e
         */
        public void actionPerformed(ActionEvent e) {
            Runnable runnable = () -> {
                int number;
                try {
                    number = Integer.parseInt(numberField.getText());
                } catch (NumberFormatException exception) {
                    JOptionPane.showMessageDialog(Window.getInstance(), "You should enter the number!", "ERROR", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                DAO dao = FileDAO.getInstance();

                switch (task){
                    case ADD:
                        dao.createElements(number);
                        break;
                    case REMOVE:
                        dao.deleteElements(number);
                        break;
                }
            };

            ThreadManager.getInstance().execute(runnable);
        }
    }
}
