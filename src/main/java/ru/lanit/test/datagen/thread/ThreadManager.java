package ru.lanit.test.datagen.thread;

import java.util.LinkedList;

/**
 * Менеджер потоков
 */
public class ThreadManager {

    /** Количество потоков */
    private final int threads_amount = 5;
    /** Очередь на обработку */
    private final LinkedList<Runnable> queue = new LinkedList<Runnable>();
    /** Потоки */
    private final Worker[] threads = new Worker[threads_amount];

    private ThreadManager () {
        for (int i = 0; i < threads_amount; i++) {
            threads[i] = new Worker();
            threads[i].start();
        }
    }

    private static ThreadManager instance;

    /**
     * Синглтон
     *
     * @return экземпляр класса ThreadManager
     */
    public static ThreadManager getInstance () {
        if (instance == null)
            instance = new ThreadManager();
        return instance;
    }

    /**
     * Выполнить задачу
     *
     * @param task задача
     */
    public void execute (Runnable task) {
        synchronized (queue) {
            queue.addLast(task);
            queue.notify();
        }
    }


    /**
     * Класс потока
     */
    private class Worker extends Thread {

        @Override
        public void run () {
            Runnable task;

            while (true) {
                synchronized (queue) {
                    while (queue.isEmpty()){
                        try {
                            queue.wait();
                        } catch (InterruptedException ignore) {}
                    }
                    task = queue.removeFirst();
                }

                try {
                    task.run();
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
